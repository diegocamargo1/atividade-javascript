//Exercicio 1

let array = [];
let num = 0;
for (let i = 0; i < 10; i++) {
  num = Math.floor(Math.random() * 100);
  array[i] = num;
};
console.log("Não ordenado");
console.log(array);

let tamanho = array.length;
let aux;

for (let i = 0; i < tamanho; i++) {
  for (let j = 0; j < tamanho; j++) {
    if (array[i] > array[j]) {
      aux = array[i];
      array[i] = array[j];
      array[j] = aux;
    }
  }
}

console.log("Ordenado");
console.log(array);

//Exercicio 2

import gods from "./gods.js";

//Q1
gods.forEach(element => { console.log(element.name, element.features.length) });

//Q2
let cont = 0;
gods.forEach(element => {
  if (element.roles.includes("Mid"))
    console.log(`${cont}:(name:${element.name}, class:${element.class}, pantheon:${element.pantheon}, damege_type:${element.damege_type}, attack_type:${element.attack_type}, roles:${element.roles}, features:${element.features})`);
  cont++;
});

//Q3
gods.sort(function (god1, god2) {
  return god1.pantheon < god2.pantheon ? -1 : 0;
});
let cont2 = 0;
gods.forEach(element => {
  console.log(`${cont2}:(name:${element.name}, class:${element.class}, pantheon:${element.pantheon}, damege_type:${element.damege_type}, attack_type:${element.attack_type}, roles:${element.roles}, features:${element.features})`);
  cont2++;
});

//Q4
let i = 0
let newGods = []
let cont3 = 0
gods.forEach(element => {
  newGods[i] = [`${element.name} (${element.class})`]
  i++
});

newGods.forEach(element => {
  console.log(`${cont3}: "${element}"`);
  cont3++;
});